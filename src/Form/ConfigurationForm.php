<?php

/**
 * @file
 * Contains the metatag access configuration form.
 */

namespace Drupal\metatag_access\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\metatag\Annotation\MetatagGroup;
use Drupal\metatag\MetatagGroupPluginManager;
use Drupal\metatag\MetatagTagPluginManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ConfigurationForm.
 *
 * @package Drupal\google_tag_manager\Form
 */
class ConfigurationForm extends ConfigFormBase {

  /**
   * The Metatag Tag Plugin Manager.
   *
   * @var \Drupal\metatag\MetatagTagPluginManager
   */
  protected $tagManager;

  /**
   * The Metatag Group Plugin Manager.
   *
   * @var \Drupal\metatag\MetatagGroupPluginManager
   */
  protected $groupManager;

  /**
   * Creates the ConfigurationForm instance.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   * @return static
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('plugin.manager.metatag.tag'),
      $container->get('plugin.manager.metatag.group')
    );
  }

  /**
   * ConfigurationForm constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   * @param \Drupal\metatag\MetatagTagPluginManager $tag_manager
   * @param \Drupal\metatag\MetatagGroupPluginManager $group_manager
   */
  public function __construct(ConfigFactoryInterface $config_factory, MetatagTagPluginManager $tag_manager, MetatagGroupPluginManager $group_manager) {
    $this->tagManager = $tag_manager;
    $this->groupManager = $group_manager;
    parent::__construct($config_factory);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'metatag_access.configuration',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'configuration_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('metatag_access.configuration');

    $required_elements = $config->get('required_elements');

    $form['required_elements'] = [
      '#type' => 'details',
      '#title' => $this->t('Required Meta Tags'),
      '#tree' => TRUE,
      '#open' => !empty($required_elements),
    ];

    /** @var MetatagGroup $group */
    foreach ($this->groupManager->getDefinitions() as $group) {
      if ($options = $this->getOptions($group['id'])) {
        $form['required_elements'][$group['id']] = [
          '#type' => 'checkboxes',
          '#title' => $group['label'],
          '#options' => $options,
          '#default_value' => isset($required_elements[$group['id']]) ? $required_elements[$group['id']] : [],
        ];
      }
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * Get metatag tags as options array.
   *
   * @param $group
   * @return array Meta tag options array.
   * Meta tag options array.
   */
  public function getOptions($group) {
    $options = [];

    foreach ($this->tagManager->getDefinitions() as $metatag) {
      if ($metatag['group'] == $group) {
        $options[$metatag['id']] = $metatag['label'];
      }
    }

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    $required = $form_state->getValue('required_elements');

    // Filter empty values.
    foreach ($required as $groupd => $tags) {
      $required[$groupd] = array_filter($tags);
      if (empty($required[$groupd])) {
        unset($required[$groupd]);
      }
    }

    $form_state->setValue('required_elements', $required);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('metatag_access.configuration')
      ->set('required_elements', $form_state->getValue('required_elements', []))
      ->save();
  }
}
