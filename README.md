Metatag Access
==============

Provides permissions to restrict administration access to single metatag tags.
Also allows to set selected meta tag form elements as required.